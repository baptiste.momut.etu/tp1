"use strict";

var data = [{
  name: 'Regina',
  base: 'tomate',
  price_small: 6.5,
  price_large: 9.95,
  image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
}, {
  name: 'Napolitaine',
  base: 'tomate',
  price_small: 6.5,
  price_large: 8.95,
  image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
}, {
  name: 'Spicy',
  base: 'crème',
  price_small: 5.5,
  price_large: 8,
  image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300'
}];
data.sort(function (a, b) {
  if (a.price_small == b.price_small) return a.price_large > b.price_large;
  return a.price_small > b.price_small;
});
var res = data.filter(function (data) {
  return data.name.split('i').length - 1 == 2;
});
var toAdd = '';
res.forEach(function (_ref) {
  var name = _ref.name,
      base = _ref.base,
      price_small = _ref.price_small,
      price_large = _ref.price_large,
      image = _ref.image;
  //const name =value.name;    
  //const url = image;
  //console.log(url);
  var html = "<article class=\"pizzaThumbnail\">\n                <a href=".concat(image, ">\n                    <img src=").concat(image, " />\n                        <section>\n                            <h4>").concat(name, "</h4>\n                            <ul>\n                                <li>Prix petit format: ").concat(price_small, " \u20AC</li>\n                                <li>Prix grand format: ").concat(price_large, " \u20AC</li> \n                            </ul>\n                        </section>\n                </a>\n            </article>");
  console.log(html);
  toAdd += html;
});
document.querySelector('.pageContent').innerHTML = toAdd;
//# sourceMappingURL=main.js.map